package fr.badcookie20.comm.server.exceptions;

public class ConnectionException extends Exception {

	private static final long serialVersionUID = -5797165187504485452L;
	
	public ConnectionException(String message) {
		super(message);
	}

}
