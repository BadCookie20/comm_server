package fr.badcookie20.comm.server;

import fr.badcookie20.comm.server.exceptions.ConnectionException;
import fr.badcookie20.comm.server.net.ConnectionsManager;

import java.util.Scanner;

public class Main {

    public static final Object VERSION = "Alpha 1.4";

    public static void main(String[] args) {
		System.out.println("Starting server...");
		try {
			ConnectionsManager manager = ConnectionsManager.init();
            manager.start();
			initCommandScanner();
		} catch (ConnectionException e) {
			e.printStackTrace();
		}
	}

    private static void initCommandScanner() {
        (new CommandScanner()).start();
    }

}
