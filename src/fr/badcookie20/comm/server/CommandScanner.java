package fr.badcookie20.comm.server;

import fr.badcookie20.comm.server.net.ConnectionsManager;

import java.io.IOException;
import java.util.Scanner;

public class CommandScanner extends Thread {

    @Override
    public void run() {
        for(;;) {
            Scanner sc = new Scanner(System.in);
            String cmd = sc.nextLine();
            if(cmd.equals("/stop")) {
                System.out.println("Stopping...");
                ConnectionsManager.getInstance().stopServer();
            }else if(cmd.equals("/threads")) {
                System.out.println("Checking threads...");
                ConnectionsManager.getInstance().checkThreads();
            }else{
                try {
                    ConnectionsManager.getInstance().publishMessage("server>" + cmd, null);
                    System.out.println("server>" + cmd);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
