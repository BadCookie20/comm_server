package fr.badcookie20.comm.server.net;

import fr.badcookie20.comm.server.Main;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;

public class ConnectionThread extends Thread {

	private Socket s;
	private ObjectInputStream ois;
	private ObjectOutputStream oos;
	private String nickname;
	private boolean stop;
	
	public ConnectionThread(Socket s) throws IOException {
		this.s = s;
		this.ois = new ObjectInputStream(s.getInputStream());
		this.oos = new ObjectOutputStream(s.getOutputStream());
		this.stop = false;
		this.nickname = s.getInetAddress().toString();
    }
	
	public Socket getSocket() {
		return this.s;
	}
	
	@Override
	public void run() {
		try {
			this.oos.writeObject(Main.VERSION);
            String nickname = (String) ois.readObject();
            if(ConnectionsManager.getInstance().isNickameUsed(nickname)) {
				this.oos.writeObject("nicknamealreadyused:" + nickname);
                this.s.close();
            }
            this.nickname = nickname;

			System.out.println("Ready to listen to " + this.nickname);

            ConnectionsManager.getInstance().publishMessageForceIgnoring(this.nickname + " vient de se connecter", this.s);

			subRun();

            this.s.close();
		} catch (ClassNotFoundException | IOException e) {
            this.stop = true;
			System.out.println(nickname + " disconnected from chat!");
            ConnectionsManager.getInstance().checkThreads();
		}
	}
	
	public void subRun() throws ClassNotFoundException, IOException {
		while(true) {
			String recieved = (String) ois.readObject();

            if(recieved.equalsIgnoreCase("disconnected")) {
                this.stop = true;
                ConnectionsManager.getInstance().publishMessage(this.nickname + " s'est déconnecté", null);
                continue;
            }

            if(recieved.equals("online")) {
                this.oos.writeObject("online:" + ConnectionsManager.getInstance().getConnections());
                continue;
            }

			if(recieved.equals("getdata")) {
                String finalString = "";
                finalString += "data:";
                finalString += ConnectionsManager.getInstance().getConnections();
                finalString += ",";
                for(ConnectionThread thread : ConnectionsManager.getInstance().getThreads()) {
                    finalString += thread.getNickName() + ";";
                }
                this.oos.writeObject(finalString);
                continue;
            }

			if(recieved.startsWith("/nickname ")) {
                String newNickname = recieved.split(" ")[1];
                if(ConnectionsManager.getInstance().isNickameUsed(newNickname)) {
                    this.sendMessage("nicknamealreadyused:" + newNickname);
                }else{
                    System.out.println(this.s.getInetAddress() + " changed his nickame from " + this.nickname + " to " + recieved.split(" ")[1]);
					this.nickname = newNickname;
					this.sendMessage("newnickname:" + this.nickname);
                }
                continue;
			}
			
			System.out.println(this.nickname + ">" + recieved);
			ConnectionsManager.getInstance().publishMessage(this.nickname + ">" + recieved, this.s);
		}
	}
	
	public String getNickName() {
		return this.nickname;
	}

    public boolean isDisconnected() {
        return this.stop || this.s.isClosed();
    }

	public void sendMessage(String message) throws IOException {
		this.oos.writeObject(message);
	}
	
}
