package fr.badcookie20.comm.server.net;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import fr.badcookie20.comm.server.exceptions.ConnectionException;

public class ConnectionsManager extends Thread {
	
	private static final int PORT = 6154;
	
	private static ConnectionsManager instance;
	
	private ServerSocket ss;
	private List<ConnectionThread> threads;
	private boolean stop;
	
	public static ConnectionsManager init() throws ConnectionException {
		try {
			ConnectionsManager manager = new ConnectionsManager(PORT); 
			instance = manager;
			return manager;
		} catch (IOException e) {
            e.printStackTrace();
			throw new ConnectionException("couldn't init the comm server. An unknown error happened");
		}
	}
	
	public ConnectionsManager(int port) throws IOException {
		this.ss = new ServerSocket(port);
		this.threads = new ArrayList<>();
		stop = false;
    }

    @Override
    public void run() {
        try {
            startServer();
        } catch (ConnectionException e) {
            e.printStackTrace();
        }
    }

    private void startServer() throws ConnectionException {
        System.out.println("Server ready!");
        while(true) {
            try {
                checkThreads();
                System.out.println("Listening...");
                Socket s = ss.accept();
                if(s == null) break;
                System.out.println("New connection from " + s.getInetAddress());
                ConnectionThread thread = new ConnectionThread(s);
                threads.add(thread);
                thread.start();
            } catch (IOException e) {
                throw new ConnectionException("couldn't start the comm server. An unknown error happened");
            }
        }

        System.out.println("Stopped the connections manager process");
    }
	
	public void stopServer() {
        try {
            publishMessage("disconnect", null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.exit(0);
	}
	
	/**
	 * Permet d'envoyer un message � tous les sockets sauf celui sp�cifi�
	 * @param message le message a envoyer. Il ne sera pas modifi�
	 * @param sendingSocket le socket qui envoie le message, null si c'est le serveur
	 * @throws IOException si une exception arrive
	 */
	public void publishMessage(String message, Socket sendingSocket) throws IOException {
		checkThreads();
		for(ConnectionThread t : threads) {
			if(sendingSocket != null && t.getSocket().equals(sendingSocket)) {
                t.sendMessage("(vous) " + message);
            }else {
                t.sendMessage(message);
            }
		}
	}

    public void publishMessageForceIgnoring(String message, Socket sendingSocket) throws IOException {
        checkThreads();
        for(ConnectionThread t : threads) {
            if(t.getSocket().equals(sendingSocket)) continue;
            t.sendMessage(message);
        }
    }

	public void checkThreads() {
        List<ConnectionThread> threads = new ArrayList<>();
        for(ConnectionThread t : ConnectionsManager.getInstance().getThreads()) {
            if(!t.isDisconnected()) {threads.add(t);
            }
        }

        ConnectionsManager.getInstance().replaceThreads(threads);
    }

	public boolean isNickameUsed(String nickname) {
        for(ConnectionThread t : this.threads) {
            if(t.getNickName().equalsIgnoreCase(nickname)) {
                return true;
            }
        }

        return false;
    }
	
	public static ConnectionsManager getInstance() {
		return instance;
	}

    public Integer getConnections() {
        System.out.println(this.threads.size());
        return this.threads.size();
    }

    public void replaceThreads(List<ConnectionThread> threads) {
        this.threads = threads;
    }

    public List<ConnectionThread> getThreads() {
        return this.threads;
    }
}
